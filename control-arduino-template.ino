#include <ros.h>
#include "Component.h"
#include "Light.h"
#include "pinout.h"

ros::NodeHandle ros_handle;

// incrementer ici
#define NB_COMPONENTS 2 

Component **components = new Component*[NB_COMPONENTS];

void setup() {
  Serial.begin(115200);
  ros_handle.getHardware()->setBaud(115200);
  delay(100);

  components[0]  = new Light(LIGHT_FRONT_PIN, 255, "front");
  components[1]  = new Light(LIGHT_REAR_PIN, 255, "rear");
  // Ajouter des nouveaux composants ici
  
  ros_handle.initNode();

  for (int i = 0; i < NB_COMPONENTS; ++i) {
    components[i]->setup_ros(ros_handle);
  }
}

void loop() {
  for (int i = 0; i < NB_COMPONENTS; ++i) {
    components[i]->update();
  }

  ros_handle.spinOnce();

  delay(10);
}
