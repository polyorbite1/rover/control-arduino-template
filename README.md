# Template à suivre pour le développement de code Arduino dans PolyOrbite Rover

## Charte technique relative à l’électronique

- Circuit haute tension : 24v 
- Circuit logique: 5v

### Base pour contrôler le hardware: 
- Arduino (Uno ou Mega idéalement)
- Connection hardware avec l’Arduino : Serial (USB)
- Connection software avec l’Arduino : rosserial (ROS1)

### Pour plus de puissance:
- Raspberry Pi 4
- RAM: 8GB
- WiFi: yes
- Type: Compute Module
- OS: Ubuntu Mate 20.04 LTS

### Pour le code :
- C++ : C++11 (le default sur Arduino IDE)
- Python : Python 3.10 (ouvert à débat/suggestions mais >=3.7)

## ROS

Version: ROS2 Foxy

Toutes communications avec l'ordinateur de bord/contrôle (Jetson ou RPi) sont faites avec ROS par serial ou wifi/ethernet.

Format des topics ROS:

`hardware_name/component_type/component_name/[get|set]/value_name`

Exemple: `mega/light/front/get/luminosity` renvoie `90`.

Mettre `get` pour tous les publishers, pour récupérer une valeur du hardware.

Mettre `set` pour tous les subscribers, pour assigner une valeur du hardware.

Salut @equipe-br :mechanical_arm: 

**Quelques liens qui peuvent être pertinents pour vous pour Ubuntu et ROS: **

Installer Ubuntu Mate 20.04 sur le Raspberry Pi (prendre ubuntu-mate-20.04.1-desktop-arm64+raspi.img.xz)
https://releases.ubuntu-mate.org/20.04/arm64/ ils ont changé de nom avant ça s’appelait Ubuntu lite

Il y a aussi ce guide d’installation mais ils sont rendu à la version 22.04 on va rester sur la version 20.04 au moins jusqu’à l’été prochain normalement
https://ubuntu-mate.org/raspberry-pi/install/

**Quelques liens pour apprendre ROS:**

ROS1 et concept de base de ROS
https://www.youtube.com/playlist?list=PLk51HrKSBQ8-jTgD0qgRp1vmQeVSJ5SQC

ROS2 (il passe pas trop de temps sur les concepts de base malheureusement) 
https://www.youtube.com/playlist?list=PLLSegLrePWgJudpPUof4-nVFHGkB62Izy
Il y a aussi des exercices/tutoriels dans les documentations officielles de ROS1 et ROS2

Comment installer ROS1 sur Raspberry Pi https://varhowto.com/install-ros-noetic-raspberry-pi-4/

Comment installer ROS2 sur Raspberry Pi https://roboticsbackend.com/install-ros2-on-raspberry-pi/

## Structure code Arduino
Le code contient 2 types de fichiers: des `.h` standards et des `.ino` qui sont l'équivalent des `.cpp` pour Arduino.

Le `main.cpp` du code est le fichier avec le même nom que le dossier, `control-arduino-template.ino` dans ce cas.

Ce main contient 2 fonctions:
- `setup()` qui initialise tout et instancie ROS
- `update()` qui est run à chaque itération

Penser a mettre un nom unique (`bras`, `science`, `controle`...) dans `Component.h`.

### Ajouter d'un nouveau système
Tous les systèmes sont à créer comme des classes indépendantes, enfantes de `Component`. 3 fonctions sont à faire:
- le constructeur: prend en argument au moins tous les numéros de pins utilisés et le nom du composant et fait toute l'initialisation du composant
- `setup_ros()`: initialise les topics ROS (publisher et subscriber)
- `update()`: fonction exécuté à chaque itération. Il est recommandé de créer différentes fonctions `update_ros()`, `update_speed()`... à exécuter dans `update()`.

Les numéros de pin sont à ajouter dans le fichier `pinout.h` avec des `#define` pour ne pas encombrer la mémoire mais rester facilement réassignable.

Une ligne avec le nouveau système est ensuite à ajouter dans le main, en incrémentant `NB_COMPONENTS`.

### A Faire
Ce serait bon de faire des classs `moteur`, `switch`... avec la structure generale.
