#pragma once

#include <ros.h>

class Component
{
  public:
    virtual void setup_ros(ros::NodeHandle &_nh) = 0;
    virtual void update() = 0;

  protected:
    // set nom du systeme ici
    String hardware_name = "mega";
};
