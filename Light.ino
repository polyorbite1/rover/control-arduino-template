#include "Light.h"

Light::Light(const unsigned int& _pinIndex, const unsigned int& _max_brightness, const String& _topic_name){
  pinIndex = _pinIndex;
  max_brightness = _max_brightness;
  pinMode(pinIndex, OUTPUT);
  turnOff();

  topic_name = _topic_name;
}

void Light::setup_ros(ros::NodeHandle &nh){ 
  String url = hardware_name+"/light/"+topic_name;
   
  publish_luminosity = new ros::Publisher((url+"/get/rpm").c_str(), &luminosity_message);
  nh.advertise(*publish_luminosity);

  light_topic_subscriber = new ros::Subscriber<std_msgs::Float64, Light>((url+"/set/intensity").c_str(), &Light::handle_ros_message, this);
  nh.subscribe(*light_topic_subscriber);
}

void Light::update(){
  //digitalWrite(pinIndex, luminosity);

  update_ros();
}

void Light::update_ros() {  
  luminosity_message.data = luminosity;
  publish_luminosity->publish(&luminosity_message);
}

void Light::turnOff(){
  setLuminosity(0);
}

void Light::setLuminosity(unsigned int _luminosity){
  luminosity = sanitizeLuminosity(_luminosity);
  analogWrite(pinIndex, luminosity);
}

unsigned int Light::sanitizeLuminosity(unsigned int& _luminosity){
  _luminosity = min(_luminosity, max_brightness);
  _luminosity = max(_luminosity, 0);
  return _luminosity;
}

int Light::getLuminosity(){
  return luminosity;
}

void Light::handle_ros_message(const std_msgs::Float64& msg){
  unsigned int ajustedValue = (unsigned int)((msg.data / 100.0) * (float)max_brightness);
  setLuminosity(ajustedValue);
}
