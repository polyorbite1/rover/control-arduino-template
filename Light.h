#pragma once

#define MORSE_SHORT_DELAY 500
#define MORSE_LONG_DELAY 1000

#include <std_msgs/Float64.h>
#include "Component.h"

class Light : public Component{
public:
  Light(const unsigned int& _pinIndex, const unsigned int& _max_brightness, const String& _topic_name);

  void setup_ros(ros::NodeHandle &_nh) override; 
  void update() override;

  void turnOff();

  void setLuminosity(unsigned int _luminosity);
  int getLuminosity();

protected:
  unsigned int sanitizeLuminosity(unsigned int& _luminosity);
  void handle_ros_message(const std_msgs::Float64& msg);
  void update_ros();

  unsigned int pinIndex;
  unsigned int luminosity;
  unsigned int max_brightness;

  String topic_name;
  std_msgs::Float64 luminosity_message;
  ros::Publisher* publish_luminosity;
  ros::Subscriber<std_msgs::Float64, Light>* light_topic_subscriber;
};
